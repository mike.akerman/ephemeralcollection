package edu.uark.util.ephemeral;

/**
 * A reference to a value and a creation time in nanoseconds
 * Useful for approximating the age of an object, provided you create the EmphemeralReference around the same time as the value.
 */
class EphemeralReference<T>
{
	final T value;
	final long created;

	EphemeralReference ( final T value )
	{
		this.value = value;
		this.created = System.nanoTime();
	}

	T get()
	{
		return value;
	}

	/**
	 * supports durations of "approximately 292 years" which is way more than enough!
	 */
	long getDurationAsNanos()
	{
		return System.nanoTime() - created;
	}
}