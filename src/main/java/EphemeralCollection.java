package edu.uark.util.ephemeral;

import java.util.*;
import java.util.concurrent.*;
import java.util.function.Supplier;

/**
 * A Collection that allows you to remove items from the collection after a certain duration.
 * For performance reasons, this is not automatic -- you must explicitly call clean() to remove expired elements from the collection.
 */
public class EphemeralCollection<T> implements Collection<T>
{
	final long duration;
	final TimeUnit unit;
	final Collection<EphemeralReference<T>> references;
	
	public EphemeralCollection( final long duration, final TimeUnit unit )
	{
		this.duration = duration;
		this.unit = unit;
		this.references = new ConcurrentLinkedQueue<EphemeralReference<T>>();
	}
	
	/**
	 * A convenience method that calls Collections.frequency() 
	 */
	public int frequency ( final Object o )
	{
		return Collections.frequency( toList(), o );
	}
	
	@Override
	public boolean add ( final T element )
	{
		return references.add( new EphemeralReference<T>(element) );
	}

	/**
	 * @return the first element in the collection
	 */
	public T addIfEmpty( final Supplier<T> supplier )
	{
		if ( isEmpty() ) add( supplier.get() );
		return iterator().next();
	}

	@Override
	public int size()
	{
		return references.size();
	}

	@Override
	public boolean isEmpty()
	{
		return references.isEmpty();
	}

	@Override
	public void clear()
	{
		references.clear();
	}
	
	/**
	 * remove all references that are older than the initially specified duration
	 * @return A pointer to "this" for fluent API purposes
	 */
	public EphemeralCollection<T> clean ()
	{
		for ( final EphemeralReference<T> reference : references )
		{
			if ( reference.getDurationAsNanos() > unit.toNanos( duration ) ) 
			{
				references.remove( reference ); //I can remove while iterating because it's a ConcurrentLinkedQueue
			}
		}
		
		return this;
	}

	@Override
	public boolean remove( final Object o )
	{
		for ( final EphemeralReference<T> reference : references ) 
		{
			if ( Objects.equals( reference.get(), o ) ) 
			{
				references.remove( reference );
				return true;
			}
		}
		
		return false;
	}

	/**
	 * @return the dereferenced EphemeralReference values
	 * Modification to the returned list will not affect the EphemeralCollection
	 */
	public List<T> toList()
	{
		final List<T> list = new LinkedList<T>();
		for ( final EphemeralReference<T> reference : references ) 
		{
			list.add( reference.get() );
		}
		return list;
	}		
	
	@Override
	public boolean contains( final Object o )
	{
		return toList().contains( o );
	}

	@Override
	public Iterator<T> iterator()
	{
		return toList().iterator();
	}

	@Override
	public Object[] toArray()
	{
		return toList().toArray();
	}

	@Override
	public <T2> T2[] toArray( final T2[] a )
	{
		return toList().toArray( a );
	}

	@Override
	public boolean containsAll( Collection<?> c )
	{
		return toList().containsAll( c );
	}

	@Override
	public boolean addAll( Collection<? extends T> c )
	{
		throw new UnsupportedOperationException( "Not supported yet." );
	}

	@Override
	public boolean removeAll( Collection<?> c )
	{
		throw new UnsupportedOperationException( "Not supported yet." );
	}

	@Override
	public boolean retainAll( Collection<?> c )
	{
		throw new UnsupportedOperationException( "Not supported yet." );
	}
}